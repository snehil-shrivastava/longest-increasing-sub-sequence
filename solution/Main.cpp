#include <bits/stdc++.h>

using namespace std;

/* The basic idea
 * eg -> {10, 22, 9, 33, 21, 50, 41, 60, 80}
 *       {{}} <- possible sol empty solutions aka sol
 *       {} <- temporary answer aka ans
 *       i => ans, sol
 *       0 =>  {10}, {{10}}
 *       1 =>  {10,22}, {{10},{22}}
 *       2 =>  {9,22}, {{10,9},{22}}
 *       3 =>  {9,22,33}, {{10,9},{22},{33}}
 *       4 =>  {9,21,33}, {{10,9},{22,21},{33}}
 *       5 =>  {9,21,33,50}, {{10,9},{22,21},{33},{50}}
 *       6 =>  {9,21,33,41}, {{10,9},{22,21},{33},{50,41}}
 *       7 =>  {9,21,33,41,60}, {{10,9},{22,21},{33},{50,41},{60}}
 *       8 =>  {9,21,33,41,60,80}, {{10,9},{22,21},{33},{50,41},{60},{80}}
 *       Observations-
 *             ∎ the length of the possible solution is the length of longest subsequence
 *             ∎ all the sub-list of sol will be sorted ; so we can use binary search again.
 *             ∎ to pick the element for our final solution in a greedy manner as shown
 *                  □ the last element should be max from the last list ; O(k)
 *                  □ then for all the next list find a value  which is just lower
 *                       then previous value of ans
 *             ie  ans[last] = 80 as 80 is the max in the last list
 *                 then 60 as 60 is just smaller then 80 so on and so forth.
 *                 sequence will be 80, 60, 50, 33, 22, 10
 *                 finally the ans will be 10, 22, 33, 50, 60, 80
 */

int main() {
  int tc, n;
  cin >> tc;
  while (tc--) {
    cin >> n;
    int arr[n];
    for (int i = 0; i < n; i++)
      cin >> arr[i];

    vector<int> ans;
    vector<vector<int>> sol;
    // doing this in o(nlogk) where k is the length of the sub sequence
    for (int x : arr) {
      // finding the next greater element of x in our answer which is empty.
      vector<int>::iterator it = upper_bound(ans.begin(), ans.end(), x);
      if (it == ans.end()) {
        // none found; add it to our answer list
        // adding {x} in all possible sol for it position
        ans.push_back(x);
        sol.push_back({x});
      } else {
        // finding the required vector for adding x to.
        int pos = it - ans.begin();
        sol[pos].push_back(x);
        *it = x;
      }
    }

    // our answer
    int size = sol.size();
    cout << size << endl;

    // finding max from last vector of possible solutions list...
    for (auto x : sol[size - 1])
      ans[size - 1] = max(x, ans[size - 1]);

    // O(nlogk)
    for (auto i = sol.end() - 2; i >= sol.begin(); i--) {
      int pos = i - sol.begin();
      // finding just smaller value of previous ans from possible solutions list...
      ans[pos] =
          *lower_bound(i->begin(), i->end(), ans[pos + 1], greater<int>());
    }

    for (auto x : ans)
      cout << x << " ";

    cout << endl;
  }
}
